
call pathogen#infect()

colorscheme ir_black

set backup
set backupdir=$HOME/.vimbackup

set nocompatible
syn on
set nu
set history=1000
set wildmenu
set smartcase
set autoindent
set expandtab
set tabstop=4
set shiftwidth=4
set laststatus=2
set showmatch
set incsearch
set hlsearch
set statusline=%<%f\ (%{&ft})\ %-4(%m%)%=%-19(%3l,%02c%03V%)


inoremap jk <esc>
inoremap <s-tab> <C-d>
let mapleader=","

augroup myfiletypes
    autocmd!
    autocmd FileType ruby,haml,eruby,yaml,html,javascript,coffee,sass,cucumber set ai sw=2 sts=2 et
    autocmd FileType python set sw=4 sts=4 et
augroup END

map <leader>e :edit %%
map <leader>v :view %%
